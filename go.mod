module gitlab.com/mizanullkirom/item-lib

go 1.16

require (
	github.com/go-chi/chi v1.5.4 // indirect
	github.com/go-redis/redis/v8 v8.11.3 // indirect
	github.com/google/uuid v1.3.0 // indirect
	go.mongodb.org/mongo-driver v1.7.2 // indirect
)
